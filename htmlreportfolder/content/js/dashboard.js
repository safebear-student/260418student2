/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7692307692307693, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "67 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "28 /"], "isController": false}, {"data": [0.0, 500, 1500, "Submit Risk"], "isController": true}, {"data": [1.0, 500, 1500, "Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "Submit Risk with file attachment"], "isController": true}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "66 /logout.php"], "isController": false}, {"data": [0.0, 500, 1500, "41 /"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "48 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "42 /reports"], "isController": false}, {"data": [1.0, 500, 1500, "Open Risk Management Menu"], "isController": true}, {"data": [1.0, 500, 1500, "58 /management/index.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 14, 0, 0.0, 2463.0, 23, 16796, 16762.5, 16796.0, 16796.0, 0.298736770228747, 1.1481235930031581, 0.4352271333006146], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["67 /index.php", 2, 0, 0.0, 30.5, 24, 37, 37.0, 37.0, 37.0, 0.08551759524522169, 0.11399562256809338, 0.03495030625988797], "isController": false}, {"data": ["28 /", 2, 0, 0.0, 161.5, 23, 300, 300.0, 300.0, 300.0, 0.08393134416047673, 0.11466792039112006, 0.03003988050274875], "isController": false}, {"data": ["Submit Risk", 2, 0, 0.0, 17241.0, 17035, 17447, 17447.0, 17447.0, 17447.0, 0.04878524734120402, 1.3124613148233972, 0.497523767562689], "isController": true}, {"data": ["Open Login Page", 2, 0, 0.0, 161.5, 23, 300, 300.0, 300.0, 300.0, 0.08338892595063375, 0.11392686270013341, 0.029845743516511005], "isController": true}, {"data": ["Submit Risk with file attachment", 2, 0, 0.0, 121.0, 96, 146, 146.0, 146.0, 146.0, 0.0850087133931228, 0.5787067783822841, 0.5128748352956177], "isController": true}, {"data": ["Login", 2, 0, 0.0, 16810.5, 16768, 16853, 16853.0, 16853.0, 16853.0, 0.04965243296921549, 0.43436181107249255, 0.1080813213753724], "isController": true}, {"data": ["66 /logout.php", 2, 0, 0.0, 54.5, 44, 65, 65.0, 65.0, 65.0, 0.08540803689627194, 0.16418527405303837, 0.06872677968996882], "isController": false}, {"data": ["41 /", 2, 0, 0.0, 16762.5, 16729, 16796, 16796.0, 16796.0, 16796.0, 0.04970178926441352, 0.26476880902087474, 0.06872825546719681], "isController": false}, {"data": ["Logout", 2, 0, 0.0, 85.0, 81, 89, 89.0, 89.0, 89.0, 0.08527330092947898, 0.2775962389144709, 0.10346882461413831], "isController": true}, {"data": ["48 /management/index.php", 2, 0, 0.0, 63.0, 59, 67, 67.0, 67.0, 67.0, 0.08511000468105026, 0.5724146506234308, 0.03549020703008639], "isController": false}, {"data": ["42 /reports", 2, 0, 0.0, 48.0, 39, 57, 57.0, 57.0, 57.0, 0.08514623866490699, 0.2912766348077824, 0.06760145706500915], "isController": false}, {"data": ["Open Risk Management Menu", 2, 0, 0.0, 63.0, 59, 67, 67.0, 67.0, 67.0, 0.0851063829787234, 0.5723902925531915, 0.03548869680851064], "isController": true}, {"data": ["58 /management/index.php", 2, 0, 0.0, 121.0, 96, 146, 146.0, 146.0, 146.0, 0.08501232678738417, 0.5787313769871631, 0.5128966356371674], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 14, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
